(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners-slides');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.slide'
        });
    };

    App.botaoTopo = function() {
        var $botao = $('#topo');
        if (!$botao.length) return;

        var posicionaBotao = function() {
            var leftPosition = ($(window).width() / 2) - (980 / 2) - 80;
            $botao.css('left', leftPosition);
        };

        $botao.on('click', function(event) {
            event.preventDefault();
            $('body,html').animate({ scrollTop: 0 }, 500);
        });

        $(window).resize(function() {
            posicionaBotao();
        }).trigger('resize');

        $(window).scroll(function() {
            if ($(this).scrollTop() > 300) {
                $botao.addClass('visible');
            } else {
                $botao.removeClass('visible');
            }
        });
    };

    App.galeriaPortfolio = function() {
        var $wrapper = $('.galeria-portfolio');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.slide',
            fx: 'scrollHorz',
            pager: '#pager',
            timeout: 0,
            pagerTemplate: ''
        });
    };

    App.videoLightbox = function() {
        var $wrapper = $('.video-fancybox');
        if (!$wrapper.length) return;

        $wrapper.fancybox({
            padding: 10,
            width: 640,
            height: 360,
            aspectRatio: true,
            scrolling: 'no',
            openEffect  : 'elastic',
            closeEffect : 'elastic'
        });
    };

    App.workshopPromocaoLightbox = function() {
        var $wrapper = $('.large-img-fancybox');
        if (!$wrapper.length) return;

        $wrapper.fancybox({
            scrolling   : 'yes',
            fitToView   : false,
            padding     : 0,
            margin      : 60,
            maxWidth    : '100%',
            openEffect  : 'elastic',
            closeEffect : 'elastic',
            helpers     : {
                title: { type: 'outside' }
            }
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        this.botaoTopo();
        this.galeriaPortfolio();
        this.videoLightbox();
        this.workshopPromocaoLightbox();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
