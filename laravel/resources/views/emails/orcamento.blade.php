<!DOCTYPE html>
<html>
<head>
    <title>[ORÇAMENTO] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    @if($tipo_evento)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Tipo de Evento:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $tipo_evento }}</span><br>
    @endif
    @if($data_evento)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Data do Evento:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $data_evento }}</span><br>
    @endif
    @if($local_cerimonia)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Local da Cerimônia:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $local_cerimonia }}</span><br>
    @endif
    @if($local_festa)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Local da Festa:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $local_festa }}</span><br>
    @endif
    @if($convidados)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Quantidade de Convidados:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $convidados }}</span><br>
    @endif
    @if($como_encontrou)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Como nos Encontrou:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $como_encontrou }}</span><br>
    @endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $mensagem }}</span>
</body>
</html>
