<?php
    $i = 0; $left = []; $right = [];
    foreach($depoimentos as $depoimento) {
        (++$i%2) ? ($left[] = $depoimento) : ($right[] = $depoimento);
    }
?>

@extends('frontend.common.template')

@section('content')

    <section id="depoimentos">
        <div class="center">
            <h2 class="title">Depoimentos</h2>

            <div class="depoimentos">
                <div class="left">
                @foreach($left as $depoimento)
                    @include('frontend.depoimentos.depoimento', compact('depoimento'))
                @endforeach
                </div>

                <div class="right">
                @foreach($right as $depoimento)
                    @include('frontend.depoimentos.depoimento', compact('depoimento'))
                @endforeach
                </div>
            </div>

            <div class="depoimentos-mobile">
                @foreach($depoimentos as $depoimento)
                    @include('frontend.depoimentos.depoimento', compact('depoimento'))
                @endforeach
            </div>

            {!! $depoimentos->render() !!}
        </div>
    </section>

@endsection
