<div class="depoimento">
    <div class="imagem">
        <img src="{{ asset('assets/img/depoimentos/'.$depoimento->imagem) }}" alt="">
    </div>
    <div class="texto">
        <div class="texto">
            {!! $depoimento->depoimento !!}
            <p class="nome">{{ $depoimento->nome }}</p>
        </div>
    </div>
</div>
