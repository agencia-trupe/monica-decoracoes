<a href="{{ asset('assets/img/promocoes/'.$promocao->imagem) }}" class="promocao large-img-fancybox" title="{!! $promocao->titulo !!}">
    <img src="{{ asset('assets/img/promocoes/thumbs/'.$promocao->imagem) }}" title="{{ $promocao->titulo }}">
    {!! $promocao->titulo !!}
</a>
