<?php
    $i = 0; $left = []; $right = [];
    foreach($promocoes as $promocao) {
        (++$i%2) ? ($left[] = $promocao) : ($right[] = $promocao);
    }
?>

@extends('frontend.common.template')

@section('content')

    <section id="promocoes">
        <div class="center">
            <h2 class="title">Promoções</h2>

            <div class="promocoes">
                <div class="left">
                @foreach($left as $promocao)
                    @include('frontend.promocoes.promocao', compact('promocao'))
                @endforeach
                </div>

                <div class="right">
                @if($video->codigo)
                    <div class="promocao promocao-video" title="{!! $video->texto !!}">
                        <div class="embed-container">
                            <iframe src="https://www.youtube.com/embed/{{ $video->codigo }}" frameborder="0" allowfullscreen></iframe>
                        </div>
                        {!! $video->texto !!}
                    </div>
                @endif
                @foreach($right as $promocao)
                    @include('frontend.promocoes.promocao', compact('promocao'))
                @endforeach
                </div>
            </div>

            <div class="promocoes-mobile">
                @foreach($promocoes as $promocao)
                    @include('frontend.promocoes.promocao', compact('promocao'))
                @endforeach
                @if($video->codigo)
                    <div class="promocao promocao-video" title="{!! $video->texto !!}">
                        <div class="embed-container">
                            <iframe src="https://www.youtube.com/embed/{{ $video->codigo }}" frameborder="0" allowfullscreen></iframe>
                        </div>
                        {!! $video->texto !!}
                    </div>
                @endif
            </div>

            {{-- $promocoes->render() --}}
        </div>
    </section>

    <style>
        .fancybox-title p { margin: 0 !important; }
        .embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
        .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
    </style>

@endsection
