    <header>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ config('site.name') }}</a></h1>
            <ul class="header-nav">
                <li>
                    <a href="{{ route('quem-somos') }}" @if(str_is('quem-somos*', Route::currentRouteName())) class='active' @endif>Quem somos</a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('portfolio') }}" @if(str_is('portfolio*', Route::currentRouteName())) class='active' @endif>Portfolio</a>
                    <ul class="header-nav-submenu">
                        @foreach($categorias as $categoria_menu)
                            <li>
                                <a href="{{ route('portfolio', $categoria_menu->slug) }}" @if(isset($categoria) && $categoria->id == $categoria_menu->id)class="active"@endif>{{ $categoria_menu->titulo }}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li>
                    <a href="{{ route('orcamento') }}" @if(str_is('orcamento*', Route::currentRouteName())) class='active' @endif>Orçamento</a>
                </li>
                <li>
                    <a href="{{ route('promocoes') }}" @if(str_is('promocoes*', Route::currentRouteName())) class='active' @endif>Promoções</a>
                </li>
                <li>
                    <a href="{{ route('videos') }}" @if(str_is('videos*', Route::currentRouteName())) class='active' @endif>Vídeos</a>
                </li>
                <li>
                    <a href="{{ route('workshops') }}" @if(str_is('workshops*', Route::currentRouteName())) class='active' @endif>Workshops</a>
                </li>
                <li>
                    <a href="{{ route('depoimentos') }}" @if(str_is('depoimentos*', Route::currentRouteName())) class='active' @endif>Depoimentos</a>
                </li>
                <li>
                    <a href="{{ route('contato') }}" @if(str_is('contato*', Route::currentRouteName())) class='active' @endif>Contato</a>
                </li>
            </ul>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>

            <ul class="header-social">
                @if($contato->facebook)
                <li>
                    <a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>
                </li>
                @endif
                @if($contato->instagram)
                <li>
                    <a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>
                </li>
                @endif
            </ul>
        </div>

        <nav id="nav-mobile">
            <a href="{{ route('quem-somos') }}" @if(str_is('quem-somos*', Route::currentRouteName())) class='active' @endif>Quem somos</a>
            <a href="{{ route('portfolio') }}" @if(str_is('portfolio*', Route::currentRouteName())) class='active' @endif>Portfolio</a>
            <a href="{{ route('orcamento') }}" @if(str_is('orcamento*', Route::currentRouteName())) class='active' @endif>Orçamento</a>
            <a href="{{ route('promocoes') }}" @if(str_is('promocoes*', Route::currentRouteName())) class='active' @endif>Promoções</a>
            <a href="{{ route('videos') }}" @if(str_is('videos*', Route::currentRouteName())) class='active' @endif>Vídeos</a>
            <a href="{{ route('workshops') }}" @if(str_is('workshops*', Route::currentRouteName())) class='active' @endif>Workshops</a>
            <a href="{{ route('depoimentos') }}" @if(str_is('depoimentos*', Route::currentRouteName())) class='active' @endif>Depoimentos</a>
            <a href="{{ route('contato') }}" @if(str_is('contato*', Route::currentRouteName())) class='active' @endif>Contato</a>
        </nav>
    </header>
