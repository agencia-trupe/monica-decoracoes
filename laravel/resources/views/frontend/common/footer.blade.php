    <footer>
        <div class="center">
            <p>
                <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                <span>·</span>
                {{ $contato->telefone }}
                @if($contato->whatsapp)
                <span>·</span>
                WhatsApp: {{ $contato->whatsapp }}
                @endif
            </p>
            <p class="copyright">
                © {{ date('Y') }} {{ config('site.name') }}
                <span>·</span>
                Todos os direitos reservados
                <span>·</span>
                <a href="http://www.trupestart.com" target="_blank">Criação de sites</a>:
                <a href="http://www.trupestart.com" target="_blank">Trupe Start</a>
            </p>
        </div>
    </footer>
    @if(Route::currentRouteName() != 'home')
    <a href="#" id="topo" title="Voltar para o Topo">Voltar para o Topo</a>
    @endif
