@extends('frontend.common.template')

@section('content')

    <section id="portfolio">
        <div class="center">
            <h2 class="title">{{ $categoria->titulo }}</h2>

            <div class="categorias-mobile">
                @foreach($categorias as $categoria_menu)
                <a href="{{ route('portfolio', $categoria_menu->slug) }}" @if(isset($categoria) && $categoria->id == $categoria_menu->id)class="active"@endif>{{ $categoria_menu->titulo }}</a>
                @endforeach
            </div>

            <div class="galeria-portfolio">
                <?php $i=0; ?>
                @foreach($categoria->imagens as $imagem)
                <div class="slide" data-cycle-hash="{{ ++$i }}" style="background-image: url('{{ asset('assets/img/portfolio/'.$imagem->imagem) }}');"></div>
                @endforeach

                <a href="#" class="galeria-control cycle-prev"></a>
                <a href="#" class="galeria-control cycle-next"></a>
            </div>
            <div class="galeria-portfolio-info">
                <div id="fb-root"></div>
                <script>
                  window.fbAsyncInit = function() {
                    FB.init({
                      appId      : '1877813332443067',
                      xfbml      : true,
                      version    : 'v2.4'
                    });
                  };

                  (function(d, s, id){
                     var js, fjs = d.getElementsByTagName(s)[0];
                     if (d.getElementById(id)) {return;}
                     js = d.createElement(s); js.id = id;
                     js.src = "//connect.facebook.net/pt_BR/sdk.js";
                     fjs.parentNode.insertBefore(js, fjs);
                   }(document, 'script', 'facebook-jssdk'));
                </script>
                <div class="fb-like" data-href="{{ Request::url() }}" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
            </div>

            <div class="thumbs-show" id="pager">
                @foreach($categoria->imagens as $thumb)
                    <a href="#">
                        <img src="{{ asset('assets/img/portfolio/thumbs/'.$thumb->imagem) }}" alt="">
                        <div class="overlay"></div>
                    </a>
                @endforeach
            </div>
        </div>
    </section>

@endsection
