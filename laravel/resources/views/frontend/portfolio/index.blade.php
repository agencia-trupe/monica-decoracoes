@extends('frontend.common.template')

@section('content')

    <section id="portfolio">
        <div class="center">
            <h2 class="title title-desktop">{{ $categoria->titulo }}</h2>
            <h2 class="title title-mobile">Portfolio</h2>

            <div class="categorias-mobile">
                @foreach($categorias as $categoria_menu)
                <a href="{{ route('portfolio', $categoria_menu->slug) }}" @if(isset($categoria) && $categoria->id == $categoria_menu->id)class="active"@endif>{{ $categoria_menu->titulo }}</a>
                @endforeach
            </div>

            <div class="thumbs-index">
            <?php $i=0; ?>
            @foreach($categoria->imagens as $imagem)
                <a href="{{ route('portfolio.show', $categoria->slug).'#'.++$i }}">
                    <img src="{{ asset('assets/img/portfolio/capa/'.$imagem->imagem) }}" alt="">
                    <div class="overlay"></div>
                </a>
            @endforeach
            </div>
        </div>
    </section>

@endsection
