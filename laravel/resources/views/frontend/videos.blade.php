@extends('frontend.common.template')

@section('content')

    <section id="videos">
        <div class="center">
            <h2 class="title">Vídeos</h2>

            <div class="thumbs">
                @foreach($videos as $video)
                <a href="http://youtube.com/embed/{{ $video->video_codigo }}?autoplay=1" class="video-fancybox fancybox.iframe">
                    <div class="capa">
                        <img src="http://img.youtube.com/vi/{{ $video->video_codigo }}/0.jpg" alt="">
                        <div class="overlay"></div>
                    </div>
                    <div class="texto">
                        <h3>{{ $video->titulo }}</h3>
                        <p>{{ $video->descricao }}</p>
                    </div>
                </a>
                @endforeach
            </div>

            {!! $videos->render() !!}
        </div>
    </section>

@endsection
