@extends('frontend.common.template')

@section('content')

    <section id="contato">
        <div class="center">
            <h2 class="title">Contato</h2>

            <div class="info">
                <img src="{{ asset('assets/img/layout/foto-contato.jpg') }}" alt="">

                <p class="telefone">{{ str_replace('·', '-', $contato->telefone) }}</p>
                <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                @if($contato->whatsapp)
                <p class="whatsapp">WhatsApp: {{ $contato->whatsapp }}</p>
                @endif
                <div class="endereco">
                    {!! $contato->endereco !!}
                </div>
            </div>

            <div class="googlemaps">
                {!! $contato->googlemaps !!}
            </div>
        </div>
    </section>

@endsection
