<a href="{{ asset('assets/img/workshops/'.$workshop->imagem) }}" class="workshop large-img-fancybox">
    <img src="{{ asset('assets/img/workshops/thumbs/'.$workshop->imagem) }}" title="{{ $workshop->titulo }}">
</a>
