<?php
    $i = 0; $left = []; $right = [];
    foreach($workshops as $workshop) {
        (++$i%2) ? ($left[] = $workshop) : ($right[] = $workshop);
    }
?>

@extends('frontend.common.template')

@section('content')

    <section id="workshops">
        <div class="center">
            <h2 class="title">Workshops</h2>

            <div class="workshops">
                <div class="left">
                @foreach($left as $workshop)
                    @include('frontend.workshops.workshop', compact('workshop'))
                @endforeach
                </div>

                <div class="right">
                @foreach($right as $workshop)
                    @include('frontend.workshops.workshop', compact('workshop'))
                @endforeach
                </div>
            </div>

            <div class="workshops-mobile">
                @foreach($workshops as $workshop)
                    @include('frontend.workshops.workshop', compact('workshop'))
                @endforeach
            </div>

            {!! $workshops->render() !!}
        </div>
    </section>

@endsection
