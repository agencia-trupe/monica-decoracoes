@extends('frontend.common.template')

@section('content')

    <section id="quem-somos">
        <div class="center">
            <h2 class="title">Quem somos</h2>

            <div class="imagem">
                <img src="{{ asset('assets/img/quem-somos/'.$quem_somos->imagem) }}" alt="">
                <div class="frase">
                    {!! $quem_somos->frase !!}
                </div>
            </div>

            <div class="texto">
                {!! $quem_somos->texto !!}
            </div>
        </div>
    </section>

@endsection
