@extends('frontend.common.template')

@section('content')

    <div class="banners">
        <div class="banners-slides">
            @foreach($banners as $banner)
            <div class="slide" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
            </div>
            @endforeach
        </div>
    </div>

@endsection
