@extends('frontend.common.template')

@section('content')

    <section id="orcamento">
        <div class="center">
            <h2 class="title">Orçamento</h2>

            <div class="info">
                <p>Consulte a disponibilidade e os valores para o seu evento preenchendo o formulário ao lado.</p>
                <p>Quanto mais detalhes você informar, melhor e mais rápido poderemos responder.</p>
                <img src="{{ asset('assets/img/layout/foto-orcamento.jpg') }}" alt="">
            </div>

            <form action="{{ route('orcamento.envio') }}" method="POST" id="form-orcamento">
                {{ csrf_field() }}

                @if(session('success'))
                <div class="response success">
                    Orçamento enviado com sucesso!
                </div>
                @elseif (count($errors) > 0)
                <div class="response error">
                    Preencha corretamente os campos obrigatórios<span>(*)</span>.
                </div>
                @endif

                <input type="text" name="nome" id="nome" placeholder="Nome*" value="{{ old('nome') }}" @if($errors->has('nome'))class="error"@endif>
                <input type="email" name="email" id="email" placeholder="E-mail*" value="{{ old('email') }}" @if($errors->has('email'))class="error"@endif>
                <input type="text" name="telefone" id="telefone" placeholder="Telefone*" value="{{ old('telefone') }}" @if($errors->has('telefone'))class="error"@endif>
                <input type="text" name="tipo_evento" id="tipo_evento" placeholder="Tipo de Evento" value="{{ old('tipo_evento') }}">
                <input type="text" name="data_evento" id="data_evento" placeholder="Data do Evento" value="{{ old('data_evento') }}">
                <input type="text" name="local_cerimonia" id="local_cerimonia" placeholder="Local da Cerimônia" value="{{ old('local_cerimonia') }}">
                <input type="text" name="local_festa" id="local_festa" placeholder="Local da Festa" value="{{ old('local_festa') }}">
                <input type="text" name="convidados" id="convidados" placeholder="Quantidade de Convidados" value="{{ old('convidados') }}">
                <input type="text" name="como_encontrou" id="como_encontrou" placeholder="Como nos encontrou" value="{{ old('como_encontrou') }}">
                <textarea name="mensagem" id="mensagem" placeholder="Mensagem*" @if($errors->has('mensagem'))class="error"@endif>{{ old('mensagem') }}</textarea>
                <input type="submit" value="Enviar">
            </form>
        </div>
    </section>

@endsection
