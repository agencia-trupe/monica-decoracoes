@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Quem Somos</h2>
    </legend>

    {!! Form::model($quem_somos, [
        'route'  => ['painel.quem-somos.update', $quem_somos->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.quem-somos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
