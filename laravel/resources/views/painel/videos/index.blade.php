@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Vídeos
            <a href="{{ route('painel.videos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Vídeo</a>
        </h2>
    </legend>

    @if(!count($videos))
    <div class="alert alert-warning" role="alert">Nenhum vídeo cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="videos">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Capa</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($videos as $video)
            <tr class="tr-row" id="{{ $video->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>
                    <img src="http://img.youtube.com/vi/{{ $video->video_codigo }}/0.jpg" alt="" style="max-width:150px;">
                </td>
                <td>{{ $video->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.videos.destroy', $video->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.videos.edit', $video->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
