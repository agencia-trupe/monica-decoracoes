@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Vídeo</h2>
    </legend>

    {!! Form::model($video, [
        'route'  => ['painel.videos.update', $video->id],
        'method' => 'patch'])
    !!}

    @include('painel.videos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
