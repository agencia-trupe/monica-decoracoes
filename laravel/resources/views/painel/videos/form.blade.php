@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::text('descricao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Html::decode(Form::label('video_codigo', 'Código do Vídeo no Youtube <small>(Exemplo: AEzGCi5AeyA)</small>')) !!}
    {!! Form::text('video_codigo', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.videos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
