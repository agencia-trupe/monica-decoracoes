@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Adicionar Vídeo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.videos.store']) !!}

        @include('painel.videos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
