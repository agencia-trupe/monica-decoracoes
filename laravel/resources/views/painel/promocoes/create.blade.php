@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Adicionar Promoção</h2>
    </legend>

    {!! Form::open(['route' => 'painel.promocoes.store', 'files' => true]) !!}

        @include('painel.promocoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
