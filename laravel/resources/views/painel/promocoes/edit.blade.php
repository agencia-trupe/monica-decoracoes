@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Promoção</h2>
    </legend>

    {!! Form::model($promocao, [
        'route'  => ['painel.promocoes.update', $promocao->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.promocoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
