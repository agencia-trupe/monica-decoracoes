@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Promoções
            <div class="btn-group pull-right">
                <a href="{{ route('painel.promocoes.video.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-film" style="margin-right:10px;"></span>Editar Vídeo</a>
                <a href="{{ route('painel.promocoes.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Promoção</a>
            </div>
        </h2>
    </legend>

    @if(!count($promocoes))
    <div class="alert alert-warning" role="alert">Nenhuma promoção cadastrada.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="promocoes">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($promocoes as $promocao)
            <tr class="tr-row" id="{{ $promocao->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><img src="{{ asset('assets/img/promocoes/'.$promocao->imagem) }}" style="width:100%;max-width:150px;" alt=""></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.promocoes.destroy', $promocao->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.promocoes.edit', $promocao->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
