@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.promocoes.index') }}" title="Voltar para Promoções" class="btn btn-sm btn-default">
        &larr; Voltar para Promoções
    </a>

    <legend>
        <h2><small>Promoções /</small> Vídeo</h2>
    </legend>

    {!! Form::model($video, [
        'route'  => ['painel.promocoes.video.update', $video->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.common.flash')

    <div class="form-group">
        {!! Html::decode(Form::label('codigo', 'Código do Vídeo no Youtube <small>(Exemplo: AEzGCi5AeyA)</small>')) !!}
        {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('texto', 'Texto') !!}
        {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>

    {!! Form::submit('Alterar', ['class' => 'btn btn-success']) !!}

    {!! Form::close() !!}

@endsection
