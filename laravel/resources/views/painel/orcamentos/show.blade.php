@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Orçamento #{{ $orcamento->id }}</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $orcamento->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $orcamento->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">{{ $orcamento->email }}</div>
    </div>

    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $orcamento->telefone }}</div>
    </div>

    @if($orcamento->tipo_evento)
    <div class="form-group">
        <label>Tipo de Evento</label>
        <div class="well">{{ $orcamento->tipo_evento }}</div>
    </div>
    @endif

    @if($orcamento->data_evento)
    <div class="form-group">
        <label>Data do Evento</label>
        <div class="well">{{ $orcamento->data_evento }}</div>
    </div>
    @endif

    @if($orcamento->local_cerimonia)
    <div class="form-group">
        <label>Local da Cerimônia</label>
        <div class="well">{{ $orcamento->local_cerimonia }}</div>
    </div>
    @endif

    @if($orcamento->local_festa)
    <div class="form-group">
        <label>Local da Festa</label>
        <div class="well">{{ $orcamento->local_festa }}</div>
    </div>
    @endif

    @if($orcamento->convidados)
    <div class="form-group">
        <label>Quantidade de Convidados</label>
        <div class="well">{{ $orcamento->convidados }}</div>
    </div>
    @endif

    @if($orcamento->como_encontrou)
    <div class="form-group">
        <label>Como nos encontrou</label>
        <div class="well">{{ $orcamento->como_encontrou }}</div>
    </div>
    @endif

    <div class="form-group">
        <label>Mensagem</label>
        <div class="well">{{ $orcamento->mensagem }}</div>
    </div>

    <a href="{{ route('painel.orcamentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
