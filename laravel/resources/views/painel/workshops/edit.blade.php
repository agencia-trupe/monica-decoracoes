@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Workshop</h2>
    </legend>

    {!! Form::model($promocao, [
        'route'  => ['painel.workshops.update', $promocao->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.workshops.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
