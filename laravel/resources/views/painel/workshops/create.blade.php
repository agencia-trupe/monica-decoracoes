@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Adicionar Workshop</h2>
    </legend>

    {!! Form::open(['route' => 'painel.workshops.store', 'files' => true]) !!}

        @include('painel.workshops.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
