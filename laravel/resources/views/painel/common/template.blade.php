<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url() }}">

    <title>{{ config('site.name') }} - Painel Administrativo</title>

    <link rel="stylesheet" href="{{ asset('assets/painel/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/painel/css/jquery-ui.min.css') }}">
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url() }}">{{ config('site.name') }}</a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                @include('painel.common.nav')
            </div>
        </div>
    </nav>

    <div class="container" style="padding-bottom:30px;">
        @yield('content')
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset("assets/jquery.min.js") }}"><\/script>')</script>
    <script src="{{ asset('assets/painel/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/painel/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/painel/js/bootbox.min.js') }}"></script>
    <script src="{{ asset('assets/painel/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/painel/jquery-file-upload/js/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('assets/painel/js/painel.js') }}"></script>
</body>
</html>
