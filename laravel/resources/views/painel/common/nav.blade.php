<ul class="nav navbar-nav" style="font-size: 14px">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>

    <li @if(str_is('painel.quem-somos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.quem-somos.index') }}">Quem Somos</a>
    </li>

    <li @if(str_is('painel.portfolio*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.portfolio.index') }}">Portfolio</a>
    </li>

    <li @if(str_is('painel.orcamentos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.orcamentos.index') }}">
        Orçamentos
        @if($orcamentos_nao_lidos >= 1)
        <span class="label label-success" style="margin-left:3px;">{{ $orcamentos_nao_lidos }}</span>
        @endif
        </a>
    </li>

    <li @if(str_is('painel.promocoes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.promocoes.index') }}">Promoções</a>
    </li>

    <li @if(str_is('painel.videos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.videos.index') }}">Vídeos</a>
    </li>

    <li @if(str_is('painel.workshops*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.workshops.index') }}">Workshops</a>
    </li>

    <li @if(str_is('painel.depoimentos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.depoimentos.index') }}">Depoimentos</a>
    </li>

    <li @if(str_is('painel.contato*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.contato.index') }}">Contato</a>
    </li>

    <li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-cog small" style="margin-right:3px"></span>
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.usuarios.index') }}">Usuários</a></li>
            <li><a href="{{ route('logout') }}">Logout</a></li>
        </ul>
    </li>
</ul>
