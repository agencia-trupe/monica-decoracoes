@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Banner</h2>
    </legend>

    {!! Form::model($banner, [
        'route'  => ['painel.banners.update', $banner->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.banners.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
