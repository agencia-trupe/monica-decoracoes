@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Portfolio /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route' => ['painel.portfolio.update', $categoria->id],
        'method' => 'patch'])
    !!}

        @include('painel.portfolio.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
