exports.development = {
    vhost  : 'monicadecoracoes.dev',
    stylus : './resources/assets/stylus/',
    js     : './resources/assets/js/',
    img    : './resources/assets/img/',
    vendor : './resources/assets/vendor/'
};

exports.build = {
    css : '../public_html/assets/css/',
    js  : '../public_html/assets/js/',
    img : '../public_html/assets/img/layout/'
};

exports.vendorPlugins = [
    this.development.vendor + 'jquery-cycle2/build/jquery.cycle2.js',
    this.development.vendor + 'fancybox/source/jquery.fancybox.pack.js',
];
