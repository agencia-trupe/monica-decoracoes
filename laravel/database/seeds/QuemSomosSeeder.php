<?php

use Illuminate\Database\Seeder;

class QuemSomosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quem_somos')->insert([
            'imagem' => '',
            'frase'  => '<p>"Realizando sonhos e surpreendendo</p><p>todos os clientes com muita</p><p>qualidade e carinho"</p>',
            'texto'  => '<p>A hist&oacute;ria da nossa empresa come&ccedil;a de forma bem espont&acirc;nea. Fui uma pessoa que sempre gostou de flores, que na &eacute;poca da escola levava broncas, por que no caminho sempre passava por casas que tinham roseiras, e dava um jeito de levar comigo. J&aacute; adulta, estava no meio dos eventos da igreja que frequentava, de casamentos de familiares e amigos, e sempre fazendo as decora&ccedil;&otilde;es.</p><p>E foi dessa forma, decorando um casamento de uma amiga, que o sal&atilde;o onde aconteceu o evento gostou do meu trabalho e me convidou para ser a decoradora de todos os eventos que estava no cronograma do sal&atilde;o. E assim, fiz v&aacute;rios cursos, me especializei . Aconteceu de forma natural, recebi apoio integral do meu irm&atilde;o Marcos, propriet&aacute;rio da empresa Studio K Fotografia e Films &nbsp;que est&aacute; h&aacute; mais de 20 anos no mercado.</p><p>Hoje temos uma empresa que tem 12 anos no mercado, com vasta experi&ecirc;ncia e extens&otilde;es no segmento de eventos com acervo atualizado exclusivo e pr&oacute;prio.<br /> &nbsp;<br /> Realizando sonhos e surpreendendo todos os clientes com muita qualidade e carinho, por que cada casamento &eacute; tratado como &uacute;nico. Gosto de sentar com a noiva e escutar o que ela deseja para o seu grande dia, e com isso fica f&aacute;cil chegar no ponto em que consigo superar as expectativas de cada casal.</p><p>Quem nos procura encontra comprometimento pleno e muito amor, seja um casamento simples ou um mega evento, tenha o estilo que for, eu vejo aquele dia como o dia mais importante na vida de um casal, ou os pais vendo a festa de 15 anos de sua filha.</p><p>A empresa Monica Decora&ccedil;&otilde;es convida voc&ecirc; para fazer parte dessa linda hist&oacute;ria.</p><p>&nbsp;</p><p style="text-align: right;"><span style="color:#5180BF">M&ocirc;nica Cristina</span></p>',
        ]);
    }
}
