<?php

use Illuminate\Database\Seeder;

class ContatoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'      => 'contato@monicadecoracoes.com.br',
            'telefone'   => '11 3683·2968',
            'whatsapp'   => '11 97013·0960',
            'endereco'   => '<p>Rua Papoula, 113 - Jd. das Flores</p><p>06110-210 - Osasco, SP</p>',
            'facebook'   => 'https://www.facebook.com/monica.decoracoes',
            'instagram'  => 'https://instagram.com/monicadecoracoes/',
            'googlemaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.979514541214!2d-46.79154039999999!3d-23.53323919999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ceffaed9da6c6d%3A0xdb381f1e818c37db!2sR.+Papoula%2C+113+-+Jardim+das+Flores%2C+Osasco+-+SP%2C+06110-210!5e0!3m2!1spt-BR!2sbr!4v1439233152107" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
        ]);
    }
}
