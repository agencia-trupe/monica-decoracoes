<?php

return [

    'name'        => 'Monica Decorações',
    'title'       => 'Monica Decorações',
    'description' => 'Projeto e decoração de festas e eventos: casamentos, debutantes, aniversários, eventos corporativos e festas em geral. Acervo próprio.',
    'keywords'    => 'decoração de eventos, decoração de festas, decoração de casamentos, decoração de festas de debutantes, decoração de festa de aniversário, decoração de eventos corporativos',
    'share_image' => 'assets/img/layout/share.jpg',
    'analytics'   => null

];
