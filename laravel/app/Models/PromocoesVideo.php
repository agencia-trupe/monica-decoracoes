<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromocoesVideo extends Model
{
    protected $table = 'promocoes_video';

    protected $guarded = ['id'];
}
