<?php

namespace App\Helpers;

class Tools
{

    public static function formataData($data = null)
    {
        if (!$data) return false;

        $meses = [
            '01' => 'jan',
            '02' => 'fev',
            '03' => 'mar',
            '04' => 'abr',
            '05' => 'mai',
            '06' => 'jun',
            '07' => 'jul',
            '08' => 'ago',
            '09' => 'set',
            '10' => 'out',
            '11' => 'nov',
            '12' => 'dez'
        ];

        list($dia, $mes, $ano) = explode('/', $data);
        return $dia . ' ' . $meses[$mes] . ' ' . $ano;
    }

    public static function formataHorario($horario)
    {
        $horario = str_replace('24', '00', $horario);

        if (strlen($horario) == 2) {
            return $horario . 'h';
        } elseif (strlen($horario) == 4) {
            return substr($horario, 0, 2) . 'h' . substr($horario, -2) . 'm';
        } else {
            return false;
        }
    }

}
