<?php

namespace App\Helpers;

use Request, Image;

class CropImage
{

    public static function make($input, $object)
    {
        if (!Request::hasFile($input) || !$object) return false;

        $image = Request::file($input);

        $name  = date('YmdHis').'_'.$image->getClientOriginalName();
        $name  = str_replace(' ', '-', $name);

        if (!is_array(array_values($object)[0])) $object = array($object);

        foreach($object as $config) {
            $width  = $config['width'];
            $height = $config['height'];
            $path   = $config['path'].$name;
            $upsize = (array_key_exists('upsize', $config) ? $config['upsize'] : false);

            if (!file_exists(public_path($config['path']))) {
                mkdir(public_path($config['path']), 0777, true);
            }

            $imgobj = Image::make($image->getRealPath());

            if (array_key_exists('galeria', $config) && $config['galeria'] == true) {
                $imgobj->resize($width, $height, function($constraint) use ($upsize) {
                    $constraint->aspectRatio();
                    if ($upsize) { $constraint->upsize(); }
                });
            } elseif ($width == null || $height == null) {
                $imgobj->resize($width, $height, function($constraint) use ($upsize) {
                    $constraint->aspectRatio();
                    if ($upsize) { $constraint->upsize(); }
                });
            } else {
                $imgobj->fit($width, $height, function ($constraint) use ($upsize) {
                    if ($upsize) { $constraint->upsize(); }
                });
            }

            if (array_key_exists('greyscale', $config) && $config['greyscale'] == true) {
                $imgobj->greyscale();
            }

            if (array_key_exists('watermark', $config) && $config['watermark'] == true) {
                $imgobj->insert('assets/img/layout/watermark.png', 'center');
            }

            $imgobj->save($path, 100);
            $imgobj->destroy();
        }

        return $name;
    }

}
