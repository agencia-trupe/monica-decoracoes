<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('banners', 'App\Models\Banner');
        $router->model('quem-somos', 'App\Models\QuemSomos');
        $router->model('portfolio', 'App\Models\Portfolio');
        $router->model('categorias', 'App\Models\PortfolioCategoria');
        $router->model('orcamentos', 'App\Models\Orcamento');
        $router->model('promocoes', 'App\Models\Promocao');
        $router->model('video', 'App\Models\PromocoesVideo');
        $router->model('videos', 'App\Models\Video');
        $router->model('workshops', 'App\Models\Workshop');
        $router->model('depoimentos', 'App\Models\Depoimento');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('imagens', function($id, $route, $model = null) {
            if ($route->hasParameter('workshops')) {
                $model = \App\Models\WorkshopImagem::find($id);
            } elseif ($route->hasParameter('portfolio')) {
                $model = \App\Models\PortfolioImagem::find($id);
            }

            return $model ?: \App::abort('404');
        });

        $router->bind('cat', function($value) {
            return \App\Models\Portfolio::slug($value)->with('imagens')->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
