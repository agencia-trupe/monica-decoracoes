<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\Orcamento;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.common.template', function() {
            $contato    = \App\Models\Contato::first();
            $categorias = \App\Models\Portfolio::ordenados()->get();

            view()->share(compact('contato', 'categorias'));
        });

        view()->composer('painel.common.template', function() {
            view()->share('orcamentos_nao_lidos', \App\Models\Orcamento::naoLidos()->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
