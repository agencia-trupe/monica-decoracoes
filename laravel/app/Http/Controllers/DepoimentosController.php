<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Depoimento;

class DepoimentosController extends Controller
{
    public function index()
    {
        $depoimentos = Depoimento::ordenados()->paginate(6);

        return view('frontend.depoimentos.index', compact('depoimentos'));
    }
}
