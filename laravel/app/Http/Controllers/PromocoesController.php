<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Promocao;
use App\Models\PromocoesVideo;

class PromocoesController extends Controller
{
    public function index()
    {
        $promocoes = Promocao::ordenados()->get();
        $video     = PromocoesVideo::first();

        return view('frontend.promocoes.index', compact('promocoes', 'video'));
    }
}
