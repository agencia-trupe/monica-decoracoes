<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Orcamento;

class OrcamentoController extends Controller
{
    public function index()
    {
        return view('frontend.orcamento');
    }

    public function envio(Request $request)
    {
        $this->validate($request, [
            'nome'     => 'required',
            'email'    => 'required|email',
            'telefone' => 'required',
            'mensagem' => 'required'
        ]);

        Orcamento::create($request->all());

        $contato = \App\Models\Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.orcamento', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[ORÇAMENTO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        if (!$request->ajax()) return \Redirect::back()->with('success', true);
    }
}
