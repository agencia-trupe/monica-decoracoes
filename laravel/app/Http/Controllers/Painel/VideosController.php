<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\VideoRequest;

use App\Http\Controllers\Controller;
use App\Models\Video;

class VideosController extends Controller
{
    public function index()
    {
        $videos = Video::ordenados()->get();

        return view('painel.videos.index', compact('videos'));
    }

    public function create()
    {
        return view('painel.videos.create');
    }

    public function store(VideoRequest $request)
    {
        try {

            $input = $request->all();

            Video::create($input);
            return redirect()->route('painel.videos.index')->with('success', 'Vídeo adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar vídeo: '.$e->getMessage()]);

        }
    }

    public function edit(Video $video)
    {
        return view('painel.videos.edit', compact('video'));
    }

    public function update(VideoRequest $request, Video $video)
    {
        try {

            $input = $request->all();

            $video->update($input);
            return redirect()->route('painel.videos.index')->with('success', 'Vídeo alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar vídeo: '.$e->getMessage()]);

        }
    }

    public function destroy(Video $video)
    {
        try {

            $video->delete();
            return redirect()->route('painel.videos.index')->with('success', 'Vídeo excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir vídeo: '.$e->getMessage()]);

        }
    }
}
