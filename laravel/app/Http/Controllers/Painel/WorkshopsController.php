<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\WorkshopRequest;

use App\Http\Controllers\Controller;
use App\Models\Workshop;
use App\Helpers\CropImage;

class WorkshopsController extends Controller
{
    private $image_config = [
        [
            'width'  => 800,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/workshops/'
        ],
        [
            'width'  => 450,
            'height' => null,
            'path'   => 'assets/img/workshops/thumbs/'
        ]
    ];

    public function index()
    {
        $workshops = Workshop::ordenados()->get();

        return view('painel.workshops.index', compact('workshops'));
    }

    public function create()
    {
        return view('painel.workshops.create');
    }

    public function store(WorkshopRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Workshop::create($input);
            return redirect()->route('painel.workshops.index')->with('success', 'Workshop adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar workshop: '.$e->getMessage()]);

        }
    }

    public function edit(Workshop $workshop)
    {
        return view('painel.workshops.edit', compact('workshop'));
    }

    public function update(WorkshopRequest $request, Workshop $workshop)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $workshop->update($input);
            return redirect()->route('painel.workshops.index')->with('success', 'Workshop alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar workshop: '.$e->getMessage()]);

        }
    }

    public function destroy(Workshop $workshop)
    {
        try {

            $workshop->delete();
            return redirect()->route('painel.workshops.index')->with('success', 'Workshop excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir workshop: '.$e->getMessage()]);

        }
    }
}
