<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\QuemSomosRequest;

use App\Http\Controllers\Controller;
use App\Models\QuemSomos;
use App\Helpers\CropImage;

class QuemSomosController extends Controller
{
    private $image_config = [
        'width'  => 400,
        'height' => 265,
        'path'   => 'assets/img/quem-somos/'
    ];

    public function index()
    {
        $quem_somos = QuemSomos::first();

        return view('painel.quem-somos.index', compact('quem_somos'));
    }

    public function update(QuemSomosRequest $request, QuemSomos $quem_somos)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $quem_somos->update($input);
            return redirect()->route('painel.quem-somos.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
