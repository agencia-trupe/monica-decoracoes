<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\DepoimentoRequest;

use App\Http\Controllers\Controller;
use App\Models\Depoimento;
use App\Helpers\CropImage;

class DepoimentosController extends Controller
{
    private $image_config = [
        'width'  => 450,
        'height' => 300,
        'path'   => 'assets/img/depoimentos/'
    ];

    public function index()
    {
        $depoimentos = Depoimento::ordenados()->get();

        return view('painel.depoimentos.index', compact('depoimentos'));
    }

    public function create()
    {
        return view('painel.depoimentos.create');
    }

    public function store(DepoimentoRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Depoimento::create($input);
            return redirect()->route('painel.depoimentos.index')->with('success', 'Depoimento adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar depoimento: '.$e->getMessage()]);

        }
    }

    public function edit(Depoimento $depoimento)
    {
        return view('painel.depoimentos.edit', compact('depoimento'));
    }

    public function update(DepoimentoRequest $request, Depoimento $depoimento)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $depoimento->update($input);
            return redirect()->route('painel.depoimentos.index')->with('success', 'Depoimento alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar depoimento: '.$e->getMessage()]);

        }
    }

    public function destroy(Depoimento $depoimento)
    {
        try {

            $depoimento->delete();
            return redirect()->route('painel.depoimentos.index')->with('success', 'Depoimento excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir depoimento: '.$e->getMessage()]);

        }
    }
}
