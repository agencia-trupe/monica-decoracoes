<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\OrcamentoRequest;

use App\Http\Controllers\Controller;
use App\Models\Orcamento;

class OrcamentosController extends Controller
{
    public function index()
    {
        $orcamentos = Orcamento::orderBy('id', 'DESC')->paginate(15);

        return view('painel.orcamentos.index', compact('orcamentos'));
    }

    public function show(Orcamento $orcamento)
    {
        $orcamento->update(['lido' => 1]);

        return view('painel.orcamentos.show', compact('orcamento'));
    }

    public function destroy(Orcamento $orcamento)
    {
        try {

            $orcamento->delete();
            return redirect()->route('painel.orcamentos.index')->with('success', 'Orçamento excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir orçamento: '.$e->getMessage()]);

        }
    }
}
