<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PortfolioImagemRequest;

use App\Http\Controllers\Controller;
use App\Models\Portfolio;
use App\Models\PortfolioImagem;

use App\Helpers\CropImage;

class PortfolioImagensController extends Controller
{
    private $image_config = [
        [
            'width'  => 180,
            'height' => 180,
            'path'   => 'assets/img/portfolio/capa/'
        ],
        [
            'width'     => 90,
            'height'    => 90,
            'greyscale' => true,
            'path'      => 'assets/img/portfolio/thumbs/'
        ],
        [
            'width'     => 980,
            'height'    => 580,
            'galeria'   => true,
            'upsize'    => true,
            'watermark' => true,
            'path'      => 'assets/img/portfolio/'
        ]
    ];
    public function index(Portfolio $portfolio)
    {
        $imagens = PortfolioImagem::portfolio($portfolio->id)->ordenados()->get();

        return view('painel.portfolio.imagens.index', compact('imagens', 'portfolio'));
    }

    public function create(Portfolio $portfolio)
    {
        return view('painel.portfolio.imagens.create', compact('portfolio'));
    }

    public function store(Portfolio $portfolio, PortfolioImagemRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $input['portfolio_id'] = $portfolio->id;

            $imagem = PortfolioImagem::create($input);
            $view = view('painel.portfolio.imagens.imagem', compact('portfolio', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Portfolio $portfolio, PortfolioImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.portfolio.imagens.index', $portfolio)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
