<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PromocaoRequest;

use App\Http\Controllers\Controller;
use App\Models\Promocao;
use App\Helpers\CropImage;

class PromocoesController extends Controller
{
    private $image_config = [
        [
            'width'  => 800,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/promocoes/'
        ],
        [
            'width'  => 450,
            'height' => null,
            'path'   => 'assets/img/promocoes/thumbs/'
        ]
    ];

    public function index()
    {
        $promocoes = Promocao::ordenados()->get();

        return view('painel.promocoes.index', compact('promocoes'));
    }

    public function create()
    {
        return view('painel.promocoes.create');
    }

    public function store(PromocaoRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Promocao::create($input);
            return redirect()->route('painel.promocoes.index')->with('success', 'Promoção adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar promoção: '.$e->getMessage()]);

        }
    }

    public function edit(Promocao $promocao)
    {
        return view('painel.promocoes.edit', compact('promocao'));
    }

    public function update(PromocaoRequest $request, Promocao $promocao)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $promocao->update($input);
            return redirect()->route('painel.promocoes.index')->with('success', 'Promoção alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar promoção: '.$e->getMessage()]);

        }
    }

    public function destroy(Promocao $promocao)
    {
        try {

            $promocao->delete();
            return redirect()->route('painel.promocoes.index')->with('success', 'Promoção excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir promoção: '.$e->getMessage()]);

        }
    }
}
