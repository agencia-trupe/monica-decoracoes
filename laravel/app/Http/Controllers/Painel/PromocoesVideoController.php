<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PromocoesVideoRequest;

use App\Http\Controllers\Controller;
use App\Models\PromocoesVideo;

class PromocoesVideoController extends Controller
{
    public function index()
    {
        $video = PromocoesVideo::first();

        return view('painel.promocoes.video', compact('video'));
    }

    public function update(PromocoesVideoRequest $request, PromocoesVideo $video)
    {
        try {

            $input = $request->all();

            $video->update($input);

            return redirect()->route('painel.promocoes.video.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
