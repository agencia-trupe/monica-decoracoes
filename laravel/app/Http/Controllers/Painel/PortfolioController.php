<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PortfolioRequest;

use App\Http\Controllers\Controller;
use App\Models\Portfolio;

class PortfolioController extends Controller
{
    public function index(Request $request)
    {
        $categorias = Portfolio::ordenados()->get();

        return view('painel.portfolio.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.portfolio.create');
    }

    public function store(PortfolioRequest $request)
    {
        try {

            Portfolio::create($request->all());
            return redirect()->route('painel.portfolio.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(Portfolio $categoria)
    {
        return view('painel.portfolio.edit', compact('categoria'));
    }

    public function update(PortfolioRequest $request, Portfolio $categoria)
    {
        try {

            $categoria->update($request->all());
            return redirect()->route('painel.portfolio.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(Portfolio $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.portfolio.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
