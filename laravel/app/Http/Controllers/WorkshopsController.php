<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Workshop;

class WorkshopsController extends Controller
{
    public function index()
    {
        $workshops = Workshop::ordenados()->paginate(9);

        return view('frontend.workshops.index', compact('workshops'));
    }

    public function show(Workshop $workshop)
    {
        return view('frontend.workshops.show', compact('workshop'));
    }
}
