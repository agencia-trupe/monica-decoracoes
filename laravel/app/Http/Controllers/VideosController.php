<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Video;

class VideosController extends Controller
{
    public function index()
    {
        $videos = Video::ordenados()->paginate(9);

        return view('frontend.videos', compact('videos'));
    }
}
