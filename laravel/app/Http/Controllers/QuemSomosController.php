<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\QuemSomos;

class QuemSomosController extends Controller
{
    public function index()
    {
        $quem_somos = QuemSomos::first();

        return view('frontend.quem-somos', compact('quem_somos'));
    }
}
