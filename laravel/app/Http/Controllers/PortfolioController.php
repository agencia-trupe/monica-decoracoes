<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Portfolio;
use App\Models\PortfolioCategoria;

class PortfolioController extends Controller
{
    public function index(Portfolio $categoria)
    {
        $categoria = !empty($categoria->id) ? $categoria : Portfolio::ordenados()->with('imagens')->first();
        if (!$categoria) \App::abort('404');

        $categorias = \App\Models\Portfolio::ordenados()->get();

        return view('frontend.portfolio.index', compact('categoria', 'categorias'));
    }

    public function show(Portfolio $categoria)
    {
        $categorias = \App\Models\Portfolio::ordenados()->get();

        view()->share(compact('categoria'));

        return view('frontend.portfolio.show', compact('categorias'));
    }
}
