<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('quem-somos', ['as' => 'quem-somos', 'uses' => 'QuemSomosController@index']);
Route::get('portfolio/{cat?}', ['as' => 'portfolio', 'uses' => 'PortfolioController@index']);
Route::get('portfolio/{cat}/galeria', ['as' => 'portfolio.show', 'uses' => 'PortfolioController@show']);
Route::get('orcamento', ['as' => 'orcamento', 'uses' => 'OrcamentoController@index']);
Route::post('orcamento', ['as' => 'orcamento.envio', 'uses' => 'OrcamentoController@envio']);
Route::get('promocoes', ['as' => 'promocoes', 'uses' => 'PromocoesController@index']);
Route::get('videos', ['as' => 'videos', 'uses' => 'VideosController@index']);
Route::get('workshops', ['as' => 'workshops', 'uses' => 'WorkshopsController@index']);
Route::get('workshops/{workshop}', ['as' => 'workshops.show', 'uses' => 'WorkshopsController@show']);
Route::get('depoimentos', ['as' => 'depoimentos', 'uses' => 'DepoimentosController@index']);
Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);


// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);

    Route::resource('banners', 'BannersController');
    Route::resource('quem-somos', 'QuemSomosController');
    // Route::resource('portfolio/categorias', 'PortfolioCategoriasController');
    Route::resource('portfolio', 'PortfolioController');
    Route::resource('portfolio.imagens', 'PortfolioImagensController');
    Route::resource('orcamentos', 'OrcamentosController');
    Route::resource('promocoes/video', 'PromocoesVideoController');
    Route::resource('promocoes', 'PromocoesController');
    Route::resource('videos', 'VideosController');
    Route::resource('workshops', 'WorkshopsController');
    // Route::resource('workshops.imagens', 'WorkshopsImagensController');
    Route::resource('depoimentos', 'DepoimentosController');
    Route::resource('contato', 'ContatoController');
    Route::resource('usuarios', 'UsuariosController');

    Route::post('order', 'HomeController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});
